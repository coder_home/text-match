# text-match

#### 介绍
使用正则表达式从文本中提取内容。

#### 安装教程

composer require php-tool/text-match

#### 使用说明

###### 使用示例

```php
include __DIR__ . '/./vendor/autoload.php';

use PHPTool\TextMatch\MatchPattern;
use PHPTool\TextMatch\Matcher;
$html = <<<E
    <!doctype html>
    <html>
        <head>
            <meta charset="utf-8" />
            <title>example</title>
        </head>
        <body>
            <div class="h">hello</div>
            <div class="w">world</div>
            <p>this is a example</p>
        </body>
    </html>
E;


$pattern = new MatchPattern();
$pattern->setLeftLimit('<{'); //设置正则标签的起始标记 (默认为 '{')
$pattern->setRightLimit('}>'); //设置正则标签的结束标记 (默认为 '}')


//设置提取规则 
$pattern->setPatterns([
    '<div <{pattern=".*?"}>><{field="words" pattern=".*?"}></div>',
    'p-tag' =>'<p><{field="describes" pattern=".*?"}></p>'
]);


$matcher = new Matcher($pattern);
$result = $matcher->match($html);
print_r($result->getData());
// print_r($result->getDataOneValue());


//替换
$content = $matcher->replace($html, ['words'=>'wordddd', 'describes'=>'hello world']);
print_r($content);
```

#### 设置提取规则

提取规则通过 **PHPTool\TextMatch\MatchPattern** 对象进行设置

```php
$pattern = new MatchPattern();
$pattern->setLeftLimit('<{'); //设置正则标签的起始标记 (默认为 '{')
$pattern->setRightLimit('}>'); //设置正则标签的结束标记 (默认为 '}')

//设置提取规则 
$pattern->setPatterns([
    '<div <{pattern=".*?"}>><{field="words" pattern=".*?"}></div>',
    '<p><{field="describes" pattern=".*?"}></p>'
]);
```

提取规则中，使用正则标签添加正则表达式，正则标签有两个属性，"pattern"是正则表达式属性，"field"是保存字段，当设置了field后，匹配结果会保存到field设置的字段中。

如果 ```pattern``` 没有设置,会被解析成 ```.*?```
 
