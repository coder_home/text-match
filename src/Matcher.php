<?php
/*****************************************************
** 匹配器
*****************************************************/
namespace PHPTool\TextMatch;

use PHPTool\TextMatch\MatchPattern;
use PHPTool\TextMatch\MatchResult;

class Matcher
{
    /**
     * pattern
     */
    public $matchPattern;

    public function __construct(MatchPattern $pattern)
    {
        $this->matchPattern = $pattern;
    }

    /**
     * 字段内容匹配
     */
    public function match($content)
    {
        $data = [];
        foreach($this->matchPattern->patternMap as $map_key=>$pattern)
        {
            preg_match_all($pattern, $content, $matches);
           
            $this->extract($map_key, $matches, $data);
        }
        return new MatchResult($data);
    }

    /**
     * 提取匹配的结果
     * @param string pattern-map-key
     * @param array $matches 匹配结果内容
     */
    protected function extract($pattern_map_key, &$matches, &$data=[])
    {
        if(isset($this->matchPattern->field_map[$pattern_map_key])){
            if(!isset($data[$this->matchPattern->field_map[$pattern_map_key]])){
                $data[$this->matchPattern->field_map[$pattern_map_key]] = [];
            }
            $data[$this->matchPattern->field_map[$pattern_map_key]] = array_merge($data[$this->matchPattern->field_map[$pattern_map_key]], $matches[0]);
        }
        foreach($this->matchPattern->field_map as $pk=>$field){
            if(!isset($data[$field])){
                $data[$field] = [];
            }
            $data[$field] = array_merge($data[$field], $matches[$pk]??[]);
        }
    }

    /**
     * 替换
     */
    public function replace($content, $replaces){
        $count = null;
        foreach($this->matchPattern->patternMap as $map_key=>$pattern)
        {
            $content = preg_replace_callback($pattern, function($matches) use($replaces){
                $res = $matches[0][0];
                $o_offset = $matches[0][1];
                $add_leng = 0;
                foreach($replaces as $field=>$replace){
                    foreach($this->matchPattern->field_map as $np=>$n_field){
                        if($field == $n_field && isset($matches[$np])){
                            // print_r($matches);
                            $offset = $matches[$np][1]+$add_leng-$o_offset;
                            $res = substr_replace($res, $replace, $offset, strlen($matches[$np][0]));
                            $add_leng += strlen($replace)-strlen($matches[$np][0]);
                        }
                    }
                }
                return $res;
            } ,$content, -1, $count, PREG_OFFSET_CAPTURE);
            
        }
        return $content;
    }
}
