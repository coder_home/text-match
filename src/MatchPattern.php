<?php

/*****************************************************
 ** Pattern
 *****************************************************/

namespace PHPTool\TextMatch;

class MatchPattern
{
    public const PATTERN_LIMIT = '/';

    /**
     * 左定界符
     */
    public $inputLeftLimit = '{';

    /**
     * 右定界符
     */
    public $inputRightLimit = '}';


    /**
     * field map
     * 用于解决名称冲突问题
     */
    public $field_map = [];

    /**
     * 字段pattern
     * 用 {field} 来表示匹配的字段内容
     */
    public $patterns = [];

    /**
     * Pattern map
     */
    public $patternMap = [];

    /**
     * 设置左定界符
     * @param string $limit
     */
    public function setLeftLimit($limit)
    {
        $this->inputLeftLimit = $limit;
    }

    /**
     * 设置右定界符
     * @param string $limit
     */
    public function setRightLimit($limit)
    {
        $this->inputRightLimit = $limit;
    }

    /**
     * 设置字段pattern
     */
    public function setPatterns(array $patterns)
    {
        $this->patterns = $patterns;
        $this->patternMap = [];
        $this->generatePatternMap();
    }

    /**
     * 生成pattern-map-key
     */
    public function generatePatternMapKey($pattern)
    {
        return md5($pattern);
    }

    /**
     * 生成pattern-map
     */
    public function generatePatternMap()
    {
        foreach ($this->patterns as $field=>$pattern) {
            $this->parsePattern($pattern, $field);
        }
    }

    /**
     * 解析接收的Pattern
     * @param string $pattern 一条接收到的pattern
     */
    public function parsePattern($pattern, $field)
    {
        $patternMapKey = $this->generatePatternMapKey($pattern);
        if(!preg_match('/^\d*$/', $field)){
            $this->field_map[$patternMapKey] = $field;
        }
        

        $left_limit = preg_quote($this->inputLeftLimit, self::PATTERN_LIMIT);
        $right_limit = preg_quote($this->inputRightLimit, self::PATTERN_LIMIT);

        $preg_pattern = '/' . $left_limit . '.*?' . $right_limit . '/s';

        $tag_patterns = [];
        preg_replace_callback($preg_pattern, function ($matches) use (&$tag_patterns) {
            $content = substr($matches[0], strlen($this->inputLeftLimit), 0 - strlen($this->inputRightLimit));

            $option = $this->parseOption($content);
            $start = '';
            $end = '';
            if ($option['field'] ?? false) {
                $pn = $this->generateSubPatternName();
                $this->field_map[$pn] = $option['field'];
                $start = '(?P<' . $pn . '>';
                $end = ')';
            }
            $p = $option['pattern'] ?? '.*?';
            $expression = $start . $p . $end;
            $tag_patterns[] = $expression;
        }, $pattern);

        $splits = preg_split($preg_pattern, $pattern);

        $result_pattern = '';
        foreach ($splits as $i => $split) {
            $result_pattern .= preg_quote($split, self::PATTERN_LIMIT);

            if (isset($tag_patterns[$i])) {
                $result_pattern .= $tag_patterns[$i];
            }
        }
        // print_r($this->patternMap);
        $this->patternMap[$patternMapKey] = self::PATTERN_LIMIT . $result_pattern . self::PATTERN_LIMIT . 's';
    }

    /**
     * 生成一个用于正则子匹配名称的字符串
     */
    public function generateSubPatternName()
    {
        return str_replace('.', '', uniqid('PN', true)) . mt_rand(100000, 999999);
    }

    /**
     * 解析正则标签选项
     */
    public function parseOption($content)
    {
        $content = trim($content);
        $patterns = [
            '/^(["\']?)[-\w]+\1=(["\']).*?\2/s',
            '/^(["\']?)[-\w]+\1=[^\'"\s]+/s',
            '/^(["\']?)[-\w]+\1(?=\s+|$)/s',
        ];
        $options = [];
        $placeholder = [];
        while ($content !== '') {
            // echo 1;
            foreach ($patterns as $pi => $p) {
                $break = false;
                $content = preg_replace_callback($p, function ($matches) use (&$break, &$options, &$placeholder, $content) {
                    // var_dump($matches);
                    $break = true;

                    $escape_limit = $this->checkEscapeLimit($matches[0]);

                    if ($escape_limit['placeholder'] !== '') {
                        $placeholder[$escape_limit['placeholder']] = $escape_limit['replace'];
                        // print_r($escape_limit['str'].PHP_EOL);
                        return $escape_limit['str'];
                    } else {
                        // var_dump($matches[0]);
                        // var_dump($content);
                        if ($placeholder) {
                            $matches[0] = str_replace(array_keys($placeholder), array_values($placeholder), $matches[0]);
                        }

                        $option = $this->optionStrToObject($matches[0]);
                        $options[$option['key']] = $option['value'];
                        return '';
                    }
                }, $content);

                $content = trim($content);

                if ($break) {
                    break;
                }
            }
            if ($break == false) {
                break;
            }
        }

        if ($content !== '') {
            // return false;
        }
        return $options;
    }

    /**
     * 检查匹配的选项边界是否为转义，并生成临时替换占位符
     */
    public function checkEscapeLimit($str)
    {
        $placeholder = '';
        $replace = '';
        $p = '/^(["\']?)[A-Za-z0-9_-]+\1=(["\']).*?\2$/s';

        if (preg_match($p, $str, $matches) == 0) {
        } else {

            $str = preg_replace_callback('/~+.$/', function ($matches) use (&$placeholder, &$replace) {
                if (strlen($matches[0]) % 2 == 1) {
                    return $matches[0];
                }
                $placeholder = uniqid('', true) . mt_rand(100000, 999999);
                $replace = substr($matches[0], -2);
                return substr($matches[0], 0, -2) . $placeholder;
            }, $str);
        }
        return compact('str', 'placeholder', 'replace');
    }

    /**
     * 将key=value解析成一个对象
     */
    public function optionStrToObject($str)
    {
        $str = trim($str);
        $offset = strpos($str, '=');
        if ($offset === false) {
            $key = trim($str, '"\'');
            $value = null;
        } else {
            $key = trim(trim(substr($str, 0, $offset)), '"\'');
            $value = trim(substr($str, $offset + 1));
            $start = substr($value, 0, 1);
            $end = substr($value, -1);
            if ($end === $start && in_array($end, ['\'', '"'])) {
                $value = substr($value, 1, -1);
                $value = preg_replace_callback('/~+?[\'"~]/', function ($matches) {
                    $arr = str_split($matches[0], 2);
                    $replace = '';
                    foreach ($arr as $item) {
                        $replace .= substr($item, -1);
                    }
                    return $replace;
                }, $value);
            }
        }
        return compact('key', 'value');
    }
}
