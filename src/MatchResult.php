<?php
/*****************************************************
** 匹配结果 
*****************************************************/
namespace PHPTool\TextMatch;

class MatchResult
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * 返回结果
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * 获取结果. 对多结果进行处理，每个字段只取一个不为空的值
     */
    public function getDataOneValue()
    {
        return array_map(function($val){
            foreach($val as $v)
            {
                if(strlen($v)>0)
                    return $v;
            }
            return '';
        }, $this->data);
    }
}
